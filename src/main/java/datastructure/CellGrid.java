package datastructure;

import java.util.Arrays;

import cellular.CellState;

public class CellGrid implements IGrid {

    private int rows;
    private int cols;
    private CellState[][] cellStates;

    public CellGrid(int rows, int columns, CellState initialState) {
		this.rows = rows;
        this.cols = columns;
        
        this.cellStates = new CellState[rows][columns];

        for (int r = 0; r < rows; r++) {
            Arrays.fill(this.cellStates[r], initialState);
        }
	}

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
        cellStates[row][column] = element;
    }

    @Override
    public CellState get(int row, int column) {
        return cellStates[row][column];
    }

    @Override
    public IGrid copy() {
        IGrid clone = new CellGrid(rows, cols, CellState.DEAD);
        
        for (int r = 0; r < rows; r++) {
            for (int c = 0; c < cols; c++) {
                clone.set(r, c, this.get(r, c));
            }
        }

        return clone;
    }
    
}
